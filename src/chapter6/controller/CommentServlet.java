package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.service.CommentService;

/*
	3. つぶやきの返信
	つぶやきに返信することができる機能を追加すること。
	トップ画面に表示しているつぶやきの下に返信を書き込める入力欄を追加する
	トップ画面で返信ボタンが押されたら、返信を登録してトップ画面を再表示する
	トップ画面に表示しているつぶやきの下に返信を表示する
 */
/*
	・	返信時は専用のサーブレットを呼び出す
	・	呼び出す際のURLは「/comment」とすること
	・	紐づいているつぶやきのIDはinputタグのhidden属性を使ってServletに送ること
 */


@WebServlet("/comment")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        String text = request.getParameter("text");
        if (!isValid(text, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
        }

        Comment comment = new Comment();
        comment.setText(text);
        comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
        comment.setUserId(Integer.parseInt(request.getParameter("userId")));

        new CommentService().insert(comment);
        response.sendRedirect("./");
    }

    private boolean isValid(String text, List<String> errorMessages) {

    	// 空白\r改行\n
        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

}
