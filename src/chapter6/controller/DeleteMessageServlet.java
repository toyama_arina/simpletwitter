package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;

/*
 * 1. つぶやきの削除
 * 自分自身のつぶやきを削除できる機能を追加すること。
 * トップ画面に表示しているつぶやきの下に削除ボタンを追加する
 * トップ画面で削除ボタンが押されたら、つぶやきを削除してトップ画面を再表示する
 */
/*
 *・	つぶやき削除時は専用のサーブレットを呼び出す
 *・	呼び出す際のURLは「/deleteMessage」とすること
 *・	削除したいつぶやきのIDはinputタグのhidden属性を使ってServletに送ること
 */
@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	// 削除実行
        new MessageService().delete(Integer.parseInt(request.getParameter("messageId")));

    	response.sendRedirect("./");

    }

}