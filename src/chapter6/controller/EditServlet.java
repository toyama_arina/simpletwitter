package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

/* 2. つぶやきの編集
	自分自身のつぶやきを編集し、更新することができる機能を追加すること。
	トップ画面に表示しているつぶやきの下に編集ボタンを追加する
	つぶやき編集画面を新規作成する
	トップ画面で編集ボタンが押されたら、つぶやき編集画面に遷移する
	つぶやき編集画面で更新ボタンが押されたら、つぶやきを更新してトップ画面に遷移する */
/*
	・	つぶやき編集時は専用のサーブレットを呼び出す
	・	呼び出す際のURLは「/edit」とすること
	・	編集したいつぶやきのIDはinputタグのhidden属性を使ってServletに送ること
	・	編集前のつぶやきをtextareaに表示すること
 */
@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	HttpSession session = request.getSession();
        String messageId = request.getParameter("messageId");
        Message message = null;

        // パラメタが正しい（数字）ときだけ、実行する
        if(!(StringUtils.isBlank(messageId)) && messageId.matches("^[0-9]*")){
        	message = new MessageService().select(Integer.parseInt(request.getParameter("messageId")));
        }

    	// 不正なパラメーター（存在しないメッセージIDを持っている場合）のアクセス
    	if(message == null) {
    		ArrayList<String> errorMessages = new ArrayList<String>();
        	errorMessages.add("不正なパラメータが入力されました");
            session.setAttribute("errorMessages", errorMessages);
        	response.sendRedirect("./");
        	return;
    	}

    	request.setAttribute("message", message);
        request.getRequestDispatcher("edit.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        String text = request.getParameter("text");
        int messageId = Integer.parseInt(request.getParameter("messageId"));
        Message message = new Message();
        message.setText(text);
        message.setId(messageId);

        if (!isValid(text, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            request.setAttribute("message", message);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
        }

        new MessageService().edit(message);
        response.sendRedirect("./");

    }

    private boolean isValid(String text, List<String> errorMessages) {

    	// 空白\r改行\n
        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

}