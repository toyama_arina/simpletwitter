package chapter6.service;

// 作成されたユーティリティクラス
// 暗号化ユーティリティー。パスワードを暗号化するために利用。
import static chapter6.utils.CloseableUtil.*;
// DB(コネクション関係)のユーティリティ。コネクションの取得、コミット、ロール バックなどを行う。
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.dao.UserDao;
import chapter6.utils.CipherUtil;

public class UserService {

    public void insert(User user) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            connection = getConnection();
            new UserDao().insert(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //
    public User select(String accountOrEmail, String password) {

        Connection connection = null;
        try {
            // パスワード暗号化
        	// DBに保存されているパスワードのデータは暗号化されているため
            String encPassword = CipherUtil.encrypt(password);

            connection = getConnection();
            User user = new UserDao().select(connection, accountOrEmail, encPassword);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User select(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, userId);
            // void chapter6.utils.DBUtil.commit(Connection connection)
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(User user) {

        Connection connection = null;
        try {

        	if(!(StringUtils.isBlank(user.getPassword()))) {
                // パスワード暗号化
                String encPassword = CipherUtil.encrypt(user.getPassword());
                user.setPassword(encPassword);
        	}

            connection = getConnection();
            new UserDao().update(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    /*
     * String型の引数をもつ、selectメソッドを追加する
     */
    public User select(String account) {

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, account);
            // エラーをキャッチした場合、コミットされない
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}